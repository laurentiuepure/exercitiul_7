public class Main {


    public static void main(String[] args) {
        String[] Receptionist = new String[5];
        Integer[] floor = new Integer[5];

        Receptionist[0] = "John Snow";
        Receptionist[1] = "William Cooper";
        Receptionist[2] = "Patricia Johnson";
        Receptionist[3] = "Kate Anderson";
        Receptionist[4] = "Dustin Rhodes";

        floor[0] = 1;
        floor[1] = 2;
        floor[2] = 3;
        floor[3] = 4;
        floor[4] = 5;

        System.out.println("The receptionist on the " + floor[0] + "st floor is " + Receptionist[0]);

        String[][] proprietar = new String[2][2];
        proprietar[0][0]= "Mr.";
        proprietar[0][1]= "Mrs.";
        proprietar[1][0]= "Smith Riley";
        proprietar[1][1]= "Catherine Jade";

        System.out.println(proprietar[0][0] + proprietar[1][0] + " and " + proprietar[0][1] + proprietar[1][1] + " are the owners of Plaza Hotel");

        char[] security = new char[]{'B', 'E', 'D', 'O', 'N', 'A', 'L', 'D', 'A', 'T', 'E'};
        char[] destination = new char [11];

        System.arraycopy(security, 2, destination, 0, 6);
        String new_dest = new String(destination);//convert array to string
        System.out.println("The hotel's security is provided by: " + new_dest);// poti folosi direct sout( "..." + new String (destination))
    }

}